# HTML

1. Criar uma tabela com os dados (para um ou mais pessoas ou personagens):
    - nome
    - idade
    - preferencias
    - ódios
    - altura
    - etc.
1. Criar um formulário simples
    - Uma entrada de texto
    - Um BTN com pesquisa nele (nada mais)

# CSS

1. Fazer o primerio exercício sem tabela - apenas com CSS.

# Bootstrap

1. Aproveitar o segundo exercício e montar um *google* customizado.
    - Ação do `form`: `action="https://google.com.br/search"`
    - Método do `form` deve ser `GET`
    - Input deve ter esse atributo: `name="q"`
