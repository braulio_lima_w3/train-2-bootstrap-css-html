# Sobre este repositório:

Contém os *moldes* que foram utilizados durante o treinamento.

Contém os exercícios que foram [sugeridos](./ex.md).

Contém as [referências](./references.md).
