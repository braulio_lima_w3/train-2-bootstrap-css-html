# Introduction

- HTML/CSS/Bootstrap (what is)
- JS included
- Why these technologies are important (namely HTML and CSS)
- Uses (outside of webdev)
- Variation

# HTML5

- box and bars
- basic:
    - p/h/em/strong/i/b/etc..
    - lists
    - div/section
    - link(a)
    - span
    - tables
        - header/body/footer
        - th/tr
        - td
    - img
    - title
- id/class
- script tag
- custom
- forms
    - input
    - select/option
    - radio
    - etc.
- execrise
    - make table like this (doesn't have to be yours...)
        - name:
        - age:
        - preferences:
        - hates:
        - height:
        - etc.
    - make a form

- meta tag
- link tag

# CSS

- style sheets
- link tag (again)
- basic stuff:
    - fonts
        - weight
        - color
        - family
    - text
        - decoration
- pq o tag/el/attr de style é ruim?
    - quebra consistencia
- placement
    - float/overflow:auto
    - (CSS3) flex box
        - essencial pro bootstrap 4
- transform
    - translate
    - rotation
    - animation
- exercise
    - using flexbox or float
    - remake the table from before using divs

- do not reinvent the wheel

# Bootstrap

- a framework filled with goodies
- discussion here: bootstrap 3 (4, however, exists)
- linking it up with the system
- in reality - very copy pasta esque
- the grid system
    - columns and whatnot
- pre built things
    - buttons
    - loading bars
    - headers
    - navbars
    - carousels
    - notification bars
    - etc.
- exercise
    - googly exercises

# Conclusion

- basic techs
- here they are
- where to go from here:
    - discover other tools
        - emmet
        - template engines (generate html + make it easier to build complex pages) (never do something else more than once)
        - scss/sass/stylus
- enjoy

