# Refências:

- HTML/CSS:
    - [Mozilla Webdev](https://developer.mozilla.org/en-US/docs/Web/Guide)
    - [W3 Schools](https://www.w3schools.com/)
    - [Flexbox Guide](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- Bootstrap
    - [v3](https://getbootstrap.com/docs/3.3/)
    - [v4](https://getbootstrap.com/docs/4.1/getting-started/introduction/)
